#!/home4/aejones/python27/.virtenv/flaskr/bin/python

###imports###
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash

###configuration###
'''
DATABASE = '/tmp/flask.db'
DEBUG = True
SECRET_KEY = '2kHOPF1aHfbKNbT'
USERNAME = 'admin'
PASSWORD = 'default'
'''
FLASKR_SETTINGS = 'flaskr.config'

###create application###
app = Flask(__name__)
#app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

if __name__ == '__main__':
    app.run()
